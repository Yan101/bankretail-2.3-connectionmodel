ALTER PROCEDURE [dbo].[SearchDebitor]
(
		@debName nvarchar(50),
		@debPostNum nvarchar(50),
		@debPhoneNum nvarchar(50)
	)
AS

	SELECT * FROM Debitors WHERE
	Name like '%' + @debName + '%' AND
	CAST (PostNumber as nvarchar(50)) like '%' + @debPostNum + '%' AND
	(PhoneNumber like '%' + @debPhoneNum + '%' OR PhoneNumber IS NULL)
 
 RETURN