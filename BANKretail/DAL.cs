﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using System.Collections;       //ArrayList
using System.Data.SqlClient;    //SqlConnection SqlCommand SqlDataReader
using System.Data.Common;       //DbDataRecord
using System.Globalization; //Для типа decimal -> в стринг, в качестве разделителя точки, а не запятой! Культурная принадлежность.
using System.IO; //Для сохранения БД в файл!
namespace BANKretail
{
    public class DAL
    {
        #region //#1 Строка соединения
        //@ - передает строку именно в таком виде в каком она написанна.
        //1 Указать имя сервера "Data Source=ИмяКомпа\SQLEXPRESS;   ; - обязательно!!!
        //2 Указать имяБД        Initial Catalog=ИмяБД;
        //3.1 Режим доступа      Integrated Security=True;" True - если исп-ем Windows-Аутентификацию, 
        //ИЛИ
        //3.2 Режим доступа      "uid=UserName;pwd=Password332;" - если исп-ем Логин/Пароль.
        #endregion
        
        string connectionString = @"Data Source = .; Initial Catalog = BANK; Integrated Security = True;";

        //#1 Данные загружаем при открытии формы в dgv_Debitors.
        public ArrayList GetAllDebitors() 
        {
            ArrayList allDebitors = new ArrayList(); //using System.Collections;
            
            //1.1 Создали и настроили обьект соединения
            using (SqlConnection con = new SqlConnection(connectionString))
            {
                //1.2 Создать и настроить обьект-комманду
                SqlCommand com = new SqlCommand("SELECT * FROM Debitors Order By Name", con);

                try
                {
                    //1.3 Вызвать метод для con 
                    con.Open(); //1.3.1 Прежде чем вызваь, необходимо установить открытое соединения с БД

                    //1.3.2 Передать команду. com.ExecuteReader();  - без возвращ параметра. 

                    //1.4 Обработать данные
                    //Посылает текст из com, по соединению con и создает обьект SqlDataReader
                    SqlDataReader dr = com.ExecuteReader(); //Через обьект dr, мы будет иметь однонаправленый доступ к чтению данных из источника данных.

                    //1.5 Пробежатся по риду
                    if (dr.HasRows) //Проверить dr на наличие записей.
                        foreach (DbDataRecord result in dr)
                            allDebitors.Add(result); //Добавляем данные в ArrayList
                }
                catch
                {

                }   
                //2. Закрыть соединение. con.Close(); или con.Dispose(); Лучше Dispose, но можно закрыть соединение ExecuteReader с перегрузкой!
            } //Из-за выхода из using - закроется соединение! Исп-ть его для обьектов поддерживающих метод Dispose.
            return allDebitors;
        }


        //#2.36 ПОИСК ИЗМЕНЕНИЯ DAL
        internal ArrayList SearchDebitors(string debName, string debPostNum, string debPhoneNum)
        {
            ArrayList searchedDebitors = new ArrayList();

            using (SqlConnection con = new SqlConnection(connectionString))
            {

                SqlCommand com = new SqlCommand("SearchDebitor", con); //1. Указываем имя нашей хранимов в БД   SearchDebitors
                com.CommandType = System.Data.CommandType.StoredProcedure; //2. Настроить Св-во CommandType
                SqlParameter param = new SqlParameter();  //3. Настроить параметры. В данном случает тока один @debitorID
                param.ParameterName = "@debName";
                param.Value = debName;
                param.SqlDbType = System.Data.SqlDbType.NVarChar;
                param.Direction = System.Data.ParameterDirection.Input; //3.1 Настроить направление параметра input или output параметер!
                com.Parameters.Add(param);  //4 Добавить!

                param = new SqlParameter();
                param.ParameterName = "@debPostNum";
                param.Value = debPostNum;
                param.SqlDbType = System.Data.SqlDbType.NVarChar;
                param.Direction = System.Data.ParameterDirection.Input; 
                com.Parameters.Add(param);

                param = new SqlParameter();
                param.ParameterName = "@debPhoneNum";
                param.Value = debPhoneNum;
                param.SqlDbType = System.Data.SqlDbType.NVarChar;
                param.Direction = System.Data.ParameterDirection.Input;
                com.Parameters.Add(param); 

                try
                {
                    con.Open();
                    SqlDataReader dr = com.ExecuteReader();

                    if (dr.HasRows)
                        foreach (DbDataRecord result in dr)
                            searchedDebitors.Add(result);
                }
                catch
                {

                }
            }
            return searchedDebitors;
        }

        //#1
        //Передаем дочернему DGV - Credits, значения из родительского DGV - Debitors, связываемся по полю ID!
        //Метод вызывается 3 раза, т.к. всего у нас 3 кредита! Это неверно - в MainForm подписатся на событие Form_Load
        internal ArrayList GetAllCreditsForDebitor(string debitorID) 
        {
            ArrayList allCredits = new ArrayList();

            using (SqlConnection con = new SqlConnection(connectionString))
            {
                //простой запрос - долго работает!
                //string query = String.Format("SELECT * FROM Credits Where DebitorID = '{0}' Order By OpenDate", debitorID); //DebitorID='{0}' не числовое значение, поэтому надо взять в ковычки ''
                //SqlCommand com = new SqlCommand(query, con);
               
                //2.31 Хранимки
                //1. Указываем имя нашей хранимов в БД
                SqlCommand com = new SqlCommand("GetAllCreditsByCurrentDebitor", con);

                //2. Настроить Св-во CommandType
                com.CommandType = System.Data.CommandType.StoredProcedure;

                //3. Настроить параметры. В данном случает тока один @debitorID
                SqlParameter param = new SqlParameter();
                param.ParameterName = "@debitorID";
                param.Value = new Guid(debitorID);
                param.SqlDbType = System.Data.SqlDbType.UniqueIdentifier;
                //3.1 Настроить направление параметра input или output параметер!
                param.Direction = System.Data.ParameterDirection.Input;
                //4
                com.Parameters.Add(param);

                try
                {
                    con.Open();
                    SqlDataReader dr = com.ExecuteReader(); 
                    
                    if (dr.HasRows) 
                        foreach (DbDataRecord result in dr)
                            allCredits.Add(result); 
                }
                catch
                {

                }   
            }
            return allCredits;
        }

        //#1 
        internal ArrayList GetAllPaymentsForCredit(string creditID)
        {
            ArrayList allPayments = new ArrayList();

            using (SqlConnection con = new SqlConnection(connectionString))
            {
                string query = String.Format("SELECT * FROM Payments Where CreditsID = '{0}' Order By PaymentDate", creditID); //DebitorID='{0}' не числовое значение, поэтому надо взять в ковычки ''
                SqlCommand com = new SqlCommand(query, con);
                try
                {
                    con.Open();
                    SqlDataReader dr = com.ExecuteReader();

                    if (dr.HasRows)
                        foreach (DbDataRecord result in dr)
                            allPayments.Add(result);
                }
                catch
                {

                }
            }
            return allPayments;
        }

        //#2 Создать нового дебитора на форме NewDebitor
        public bool SaveNewDebitor(string ID, string Name,
            string PostNumber, string PhoneNumber)
        {
            bool flagResult = false;
            string query = string.Format("INSERT INTO Debitors " +
                " ([ID], [Name], [PostNumber], [PhoneNumber]) " +
                "VALUES ('{0}','{1}','{2}','{3}')",
                ID, Name, PostNumber, (PhoneNumber != String.Empty) ? PhoneNumber : null);
            using (SqlConnection con = new SqlConnection(connectionString))
            {
                SqlCommand com = new SqlCommand(query, con);
                try 
                {
                    con.Open();
                    if (com.ExecuteNonQuery() == 1)  //Просто передает команду в БД, и все! Замена - ExecuteReader'у Возвращается к-во обработанных записей!
                        flagResult = true;
                }
                catch { }
            }
            return flagResult;
        }

        //#2 Создание нового кредита DAL.  BLL должен принять данные и переконвертировать их и отдать DAL
        public bool SaveNewCredit(Guid ID, Guid debitorID, int amount, 
            int balance, DateTime openDate)
        {
            //2.30 Параметризированный запрос работает быстрее ,чем string'овые запросы. @ID, @debitorID
            //Нужен для строгой типизиции, чтобы при вводе не ввести случайно команду SQL , Drop DB и удалить БД :)
            bool flagResult = false;
            string query = String.Format("INSERT INTO Credits" +
                "(ID, DebitorID, Amount, Balance, OpenDate)" +
                "VALUES (@ID, @debitorID, @Amount, @Balance, @OpenDate)"); //1.Создали параматры

            using (SqlConnection con = new SqlConnection(connectionString))
            {
                SqlCommand com = new SqlCommand(query, con);

                SqlParameter param = new SqlParameter(); //2. Можно перегрузить конструктор, можно через Св-ва.
                param.ParameterName = "@ID";                                //Имя
                param.Value = ID;                                           //Значение
                param.SqlDbType = System.Data.SqlDbType.UniqueIdentifier;   //Тип ..bool SaveNewCredit(Guid ID,...
                com.Parameters.Add(param);              //3. Заносим параметр в SqlCommand обьект!

                param = new SqlParameter("@debitorID", debitorID); //2. через Св-ва
                param.SqlDbType = System.Data.SqlDbType.UniqueIdentifier;
                com.Parameters.Add(param);

                param = new SqlParameter();
                param.ParameterName = "@Amount";
                param.Value = amount;
                param.SqlDbType = System.Data.SqlDbType.Money;
                com.Parameters.Add(param);

                param = new SqlParameter();
                param.ParameterName = "@Balance";
                param.Value = balance;
                param.SqlDbType = System.Data.SqlDbType.Money;
                com.Parameters.Add(param);

                param = new SqlParameter();
                param.ParameterName = "@OpenDate";
                param.Value = openDate;
                param.SqlDbType = System.Data.SqlDbType.DateTime;
                com.Parameters.Add(param); 

                try
                {
                    con.Open();
                    if (com.ExecuteNonQuery() == 1)  //Просто передает команду в БД, и все! Замена - ExecuteReader'у Возвращается к-во обработанных записей!
                        flagResult = true;
                }
                catch (Exception)
                {

                }
            }
            return flagResult;
        }

        //2.24 2.25 Создание нового платежа DAL. ТРАНЗАКЦИИ
        internal bool SaveNewPayment(Guid ID, Guid creditID,
            decimal paymentAmount, DateTime dateTime)
        {
            bool flag = true;

            using (SqlConnection con = new SqlConnection(connectionString))
            {
                con.Open(); //Открыть соединение.
                //Преимущество транзакции - целостное сохранение данных. Если во время выполения транзакции будут ошибки, то будет откат!
                SqlTransaction sqlTransact = con.BeginTransaction(); //Начала транзакции! Указали БД, что мы перешли в режим транзакции. Несколько комманд либо выполнятся, либо нет - все вместе!
                SqlCommand com = con.CreateCommand();
                com.Transaction = sqlTransact; //Настроим св-во. Установим его в sqlTransact. Назначить транзакцию ту, которую только что создали

                try
                {
                    string payAmount = paymentAmount.ToString(CultureInfo.InvariantCulture.NumberFormat); //Указали в тип string, что конвертируемое значение должно быть предоставлено в виде числового формата!
                    //Создали платеж на некую сумму!
                    string query = string.Format("INSERT INTO Payments (ID, CreditsID, Amount, PaymentDate) VALUES " +
                        "('{0}', '{1}', '{2}', '{3}')", ID, creditID, payAmount, dateTime); //payAmount - тип стринг, но запятая изменится на точку!
                    com.CommandText = query; //Установили вручную, т.к. создавали не через конструктор
                     com.ExecuteNonQuery();

                    //Уменьшили Баланс на некую сумму!
                    query = string.Format("UPDATE Credits SET Balance = (Balance - {0}) WHERE ID = '{1}'",
                        payAmount, creditID);
                    com.CommandText = query; //назначить
                    com.ExecuteNonQuery();

                    //Зафиксирвоать эти значения по транзакции
                    sqlTransact.Commit(); //Закрыть транзакцию.
                }
                catch(Exception)
                {
                    sqlTransact.Rollback(); //Если ошибка времени выполнения танзакции, то откатить транзакцию!!!
                    flag = false;
                }
                finally
                {
                    if (con.State == System.Data.ConnectionState.Open)
                    {
                        con.Dispose();
                    }
                }
            }
            return flag;
        }

        //2.27 Сохранение данных. и проверка
        internal bool SaveDBToLocalFile()
        {
            bool result = true;
            //Обьект создается через new потому что один раз его создаем, и не надо держать его в стеке ссылку на него!
            //Создали файл Debitors.csv при помощи FileMode.Create с кодировкой имени Encoding.GetEncoding(1251) и получили из него тип FileStream.
            //В поток файла будет записывать данные, а потом сохраняем это все в файл.  
            //Работа с файлом через ссылку file типа StreamWriter, если бы создавали первый тогда просто в параметрах FileStream, без new!
            StreamWriter file;
            //Заросы кБД
            string query;
            using (SqlConnection con = new SqlConnection(connectionString))
            {
                try
                {
                    file = new StreamWriter(new FileStream("Debitors.csv", FileMode.Create), Encoding.GetEncoding(1251));
 
                    query = "SELECT * FROM Debitors";
                    SqlCommand com = new SqlCommand(query, con);
                    con.Open();
                    SqlDataReader reader = com.ExecuteReader();

                    file.WriteLine("Start of file.");
                    file.WriteLine(@"""ID"";""Name"";""PostNumber"";""PhoneNumber"""); //Надпись внутри ячейки. ; - переход к след ячейке "" Name "" - имя ячейки

                    //Заносим данные в файл.
                    if (reader.HasRows)
                    {
                        //чатсо исп-ся с ридером!
                        while (reader.Read())
                        {
                            file.WriteLine(@"""" + reader.GetValue(0).ToString() + @""";""" + //GetValue принимает значение ячейки ID по индексу 0
                                reader.GetString(1) + @""";""" +
                                reader[2].ToString() + @""";""" + //Через индексатор
                                reader[3].ToString() + @"""", Encoding.ASCII);
                        }
                    }
                    else
                    {
                        file.WriteLine("No one row to save.");
                    }

                    file.WriteLine("End of file.");
                    result = true;
                    file.Dispose(); //Закрыть файл.
                }
                catch (Exception)
                {
                    result = false;
                    return result;
                }
            }

            //2.29 записываем кредиторов в файл!

            using (SqlConnection con = new SqlConnection(connectionString))
            {
                try
                {
                    //Вдруг нету прав на создание файла!
                    file = new StreamWriter(new FileStream("Credits.csv", FileMode.Create), Encoding.GetEncoding(1251));

                    query = "SELECT * FROM Credits";
                    SqlCommand com = new SqlCommand(query, con);
                    con.Open();
                    SqlDataReader reader = com.ExecuteReader();

                    file.WriteLine("Start of file.");
                    file.WriteLine(@"""ID"";""DebitorID"";""Amount"";""Balance"";""OpenDate"""); //Надпись внутри ячейки. ; - переход к след ячейке "" Name "" - имя ячейки

                    //Заносим данные в файл.
                    if (reader.HasRows)
                    {
                        //чатсо исп-ся с ридером!
                        while (reader.Read())
                        {
                            file.WriteLine(@"""" + reader[0].ToString() + @""";""" + //GetValue принимает значение ячейки ID по индексу 0
                                reader[1].ToString() + @""";""" +
                                reader[2].ToString() + @""";""" + //Через индексатор
                                reader[3].ToString() + @"""" +
                                reader[4].ToString() + @"""", Encoding.ASCII);
                        }
                    }
                    else
                    {
                        file.WriteLine("No one row to save.");
                    }

                    file.WriteLine("End of file.");
                    result = true;
                    file.Dispose(); //Закрыть файл.
                }
                catch (Exception)
                {
                    result = false;
                    return result;
                }
            }

            //2.29 записываем Платежи в файл!
            using (SqlConnection con = new SqlConnection(connectionString))
            {
                try
                {
                    file = new StreamWriter(new FileStream("Payments.csv", FileMode.Create), Encoding.GetEncoding(1251));

                    query = "SELECT * FROM Payments";
                    SqlCommand com = new SqlCommand(query, con);
                    con.Open();
                    SqlDataReader reader = com.ExecuteReader();

                    file.WriteLine("Start of file.");
                    file.WriteLine(@"""ID"";""CreditID"";""Amount"";""PaymentDate"""); //Надпись внутри ячейки. ; - переход к след ячейке "" Name "" - имя ячейки

                    //Заносим данные в файл.
                    if (reader.HasRows)
                    {
                        //чатсо исп-ся с ридером!
                        while (reader.Read())
                        {
                            file.WriteLine(@"""" + reader[0].ToString() + @""";""" + //GetValue принимает значение ячейки ID по индексу 0
                                reader[1].ToString() + @""";""" +
                                reader[2].ToString() + @""";""" + //Через индексатор
                                reader[3].ToString() + @"""", Encoding.ASCII);
                        }
                    }
                    else
                    {
                        file.WriteLine("No one row to save.");
                    }

                    file.WriteLine("End of file.");
                    result = true;
                    file.Dispose(); //Закрыть файл.
                }
                catch (Exception)
                {
                    result = false;
                    return result;
                }
            }
            return result;
        }
    }
}