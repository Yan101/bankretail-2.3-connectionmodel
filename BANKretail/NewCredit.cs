﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;

using System.Collections;

namespace BANKretail
{
    public partial class NewCredit : Form
    {
        ArrayList allDebitors;
        DAL dal = new DAL();
        public NewCredit()
        {
            InitializeComponent();
            txbx_creditID.Text = Guid.NewGuid().ToString();

            allDebitors= dal.GetAllDebitors(); //Чтобы 2 раза не лазить в БД!

            if (allDebitors == null||allDebitors.Count == 0) //Заблокировать кнопку и поля, если пустой список Дебиторов в Кредитах.
            {
                txbx_creditBalance.Enabled =
                    txbx_creditAmount.Enabled =
                    btn_saveNewCredit.Enabled = false;
            }
            //Заполняем листБоксы
            ltbx_debitorID.DataSource = allDebitors;
            ltbx_debitorName.DataSource = allDebitors;

        }

        //#2 Дублируем записи!
        private void txbx_creditAmount_TextChanged(object sender, EventArgs e)
        {
            txbx_creditBalance.Text = txbx_creditAmount.Text;
        }

        //#2 Сохраняем новый кредит для пользователя!
        private void btn_saveNewCredit_Click(object sender, EventArgs e)
        {   //Т.к решили сразу конвертировать данные, чтобы DAL их принимал готовыми и не конвертировал из стринга! 
            //new Guid(txbx_creditID.Text - конвертировать стринг в Guid!
            if (dal.SaveNewCredit(new Guid(txbx_creditID.Text), new Guid(ltbx_debitorID.SelectedValue.ToString()),
                Int32.Parse(txbx_creditAmount.Text), Int32.Parse(txbx_creditBalance.Text), dtp_creditOpenDate.Value))
                this.DialogResult = DialogResult.OK;
            else
                this.DialogResult = DialogResult.No;
        }

        //#2 Какой символ был нажат!
        private void txbx_creditAmount_KeyPress(object sender, KeyPressEventArgs e)
        {
            if ((e.KeyChar < 48 || e.KeyChar > 57) && e.KeyChar != 8) //KeyChar Числовое значение введенное с клавиатуры. числа из 48-57!! И доп  != 8 - это чтобы BackSpace работал! !!!!
                e.Handled = true; //Событие ввода символа обработали, но ничего с ним не сделали! Т.е. данный символ не привется валидным для тек тексбокса!

        }

        private void txbx_creditAmount_Leave(object sender, EventArgs e)
        {
            if (txbx_creditAmount.Text == string.Empty ||
                Int64.Parse(txbx_creditAmount.Text.Trim()) <  100 || //Parse - когда уверенны что вводим сразу числа!
                Int64.Parse(txbx_creditAmount.Text.Trim()) >  100000000)
            {
                lbl_messageCreditAmount.Text = "Недопустимое значение суммы кредита!";
                lbl_messageCreditAmount.ForeColor = Color.Red;
                btn_saveNewCredit.Enabled = false;
            }
            else
            {
                lbl_messageCreditAmount.Text = "Сумма допустима.";
                lbl_messageCreditAmount.ForeColor = Color.Green;
                btn_saveNewCredit.Enabled = true;
            }
        }
    }
}
