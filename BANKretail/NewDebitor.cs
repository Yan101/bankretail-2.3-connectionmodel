﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;

namespace BANKretail
{
    //#2
    public partial class NewDebitor : Form
    {
        DAL dal = new DAL();
        public NewDebitor()
        {
            //Контролы создаются после этого метода!
            InitializeComponent();

            //Автоматом генерирует ID - уникального идентификатора, в БД у него тип не int, а unique.... 32 символа!
            txbx_debitorID.Text = Guid.NewGuid().ToString();

        }
        //#2 Сохранение нового дебитора DAL
        private void btn_saveNewDebitor_Click(object sender, EventArgs e)
        {
            //Занести нового дебитора в БД
            if (dal.SaveNewDebitor(txbx_debitorID.Text.Trim(),
                txbx_debitorName.Text.Trim(),
                txbx_debitorPostNumber.Text.Trim(),
                txbx_PhoneNumber.Text.Trim()))
            this.DialogResult = DialogResult.OK; //Модальный режим, блокирует основную форму. Главное окно запущено в режиме ShowDialog и ждет пока не будет OK.
            
            else
                this.DialogResult = DialogResult.No;

        }
    }
}
