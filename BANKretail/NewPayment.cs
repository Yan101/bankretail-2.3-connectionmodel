﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;

using System.Collections;
namespace BANKretail
{
    public partial class NewPayment : Form
    {
        DAL dal = new DAL();
        //Создать ArrayList для всех дебиторов и для всех кредитов выданных данному дебитору
        ArrayList allDebitors;
        ArrayList allCredits;

        public NewPayment()
        {
            InitializeComponent();
            txbx_paymentID.Text = Guid.NewGuid().ToString();

            allDebitors = dal.GetAllDebitors();
            if (allDebitors == null || allDebitors.Count == 0)
            {
                btn_saveNewPayment.Enabled = false;
                txbx_paymentAmount.Enabled = false;
            }
            else
            {
                btn_saveNewPayment.Enabled = true;
                txbx_paymentAmount.Enabled = true;
            }

            ltbx_debitorID.DataSource = allDebitors;
            ltbx_debitorName.DataSource = allDebitors;
        }

        //Изменили ltbx_debitorID на ltbx_debitor - чтобы подписатся несколькими listbox'ами!
        private void ltbx_debitor_SelectedIndexChanged(object sender, EventArgs e)
        {
            allCredits = dal.GetAllCreditsForDebitor(ltbx_debitorID.SelectedValue.ToString());
            ltbx_creditID.DataSource = allCredits;
            if (allCredits == null || allCredits.Count == 0)
            {
                btn_saveNewPayment.Enabled = false;
                txbx_paymentAmount.Enabled = false;
            }
            else
            {
                btn_saveNewPayment.Enabled = true;
                txbx_paymentAmount.Enabled = true;
            }
            ltbx_creditAmount.DataSource = allCredits;
            ltbx_creditBalance.DataSource = allCredits;

            //Назначаем каждому листБоксу ValueMember и DisplayMember
            ltbx_creditID.DisplayMember = "ID";
            ltbx_creditID.ValueMember = "ID";
            ltbx_creditAmount.DisplayMember = "Amount";
            ltbx_creditAmount.ValueMember = "ID";
            ltbx_creditBalance.DisplayMember = "Balance";
            ltbx_creditBalance.ValueMember = "Balance"; //Вместо ID установим Balance, чтобы получать баланс! Это нужно для проверки вводимой суммы!
        }

        private void txbx_paymentAmount_KeyPress(object sender, KeyPressEventArgs e)
        {
            //Проверка на запятую!
            if (e.KeyChar == ',')
            {
                if (txbx_paymentAmount.Text.Trim().Contains(',') ||     //Нельзя вводить запятую второй раз! Если вся строка содержит запятую! 
                       txbx_paymentAmount.Text == String.Empty)         //Нельзя вводить запятую вначале, в пустое поле!  
                {
                    e.Handled = true; //не позволяем вводить символ. WTF???
                    return; //выйти из метода
                }
                else
                {
                    e.Handled = false;
                    return;
                }
            }

            //Если не запятая то надо проверить, что это числовой символ!
            short res;
            if (Int16.TryParse(e.KeyChar.ToString(), out res))
                e.Handled = false;
            else
                e.Handled = true;
        }

        //2.23
        private void txbx_paymentAmount_Leave(object sender, EventArgs e)
        {
            //проверка на пустое значение
            if (txbx_paymentAmount.Text.Trim() == String.Empty )
            {
                lbl_message.Text = "Сумма платежа не введена!";
                lbl_message.ForeColor = Color.Red;
                btn_saveNewPayment.Enabled = false;
                return; //чтобы не попадать в блок который НИЖЕ !! 
            }
            
            //Сумма платежа в рамках > 100$ и не больше чем есть на балансе!!!!
            decimal payValue = decimal.Parse(txbx_paymentAmount.Text.Trim());
            if (payValue < 100 || 
                payValue > decimal.Parse(ltbx_creditBalance.SelectedValue.ToString())) //Выбранная текущая запись. SelectedValue. Получаем  
            {
                lbl_message.Text = "Сумма платежа не валидна!";
                lbl_message.ForeColor = Color.Red;
                btn_saveNewPayment.Enabled = false;
            }
            else
            {
                lbl_message.Text = "Сумма платежа валидна";
                lbl_message.ForeColor = Color.Green;
                btn_saveNewPayment.Enabled = true;
            }
        }

        private void btn_refresh_Click(object sender, EventArgs e)
        {
            txbx_paymentAmount.Text = "";
        }

        //2.24
        private void btn_saveNewPayment_Click(object sender, EventArgs e)
        {
            //еще раз проверим валидность суммы
            //decimal.TryParse - валидин ли тип для decimal или нет!
            decimal paymentAmount;
            if (!decimal.TryParse(txbx_paymentAmount.Text.Trim(), out paymentAmount)) //Если тип не валиден, проверка проходится!
            {
                MessageBox.Show("Неверно указанна сумма платежа");
                return;
            }
            //Если в текстовом поле находятся валидные данные
            if (dal.SaveNewPayment(new Guid(txbx_paymentID.Text.Trim()),
                new Guid(ltbx_creditID.SelectedValue.ToString()),
                paymentAmount, dtp_paymentPassDate.Value)) //Если не прошли проверку выше, то можно исп-ть знач paymentAmount
                this.DialogResult = DialogResult.OK;    //На уровне DAL 
            else
                this.DialogResult = DialogResult.No;    //На уровне DAL 
        }
    }
}
