﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;

using System.Collections;

namespace BANKretail
{
    public partial class MainForm : Form
    {
        DAL dal = new DAL(); //#1 ссылка на DAL класс.
        ArrayList allDebitors; //Для галочки CHBX! 2.37!
        public MainForm() //Уровни Presentable и BLL
        {
            InitializeComponent();

            //dgv_debitors.DataSource = dal.GetAllDebitors();   //#1
            allDebitors = dal.GetAllDebitors();                 //2.37
            dgv_debitors.DataSource = allDebitors;              //2.37
            SettingsDGV_Debitors();                             //#2 Скрываем колонку ID PostNumber PhoneNumber в DGV_Debitors!
        }

        //#1 Скрываем из DGV_Debitors лишние колонки!
        void SettingsDGV_Debitors()
        {
            try //Если ошибка в названии колонки, то ввывести все колонки
            {
                //Обращаемся к колонкам DGV по индексу
                dgv_debitors.Columns["ID"].Visible = false; ////dgv_debitors.Columns[0].Visible = false;
                dgv_debitors.Columns["PostNumber"].Visible = false;
                dgv_debitors.Columns["PhoneNumber"].Visible = false;

                dgv_debitors.TopLeftHeaderCell.Value = "#"; //Установили знак Ячейке верхней левой!
            }
            catch (Exception)
            {

            }
        }

        //#1
        private void MainForm_Load(object sender, EventArgs e) //При загрузке формы подписываемся на события CellEnter, чтобы не подключатся заново к БД!! 
        {
            dgv_debitors.CellEnter += new DataGridViewCellEventHandler(dgv_debitors_CellEnter); //#1
            dgv_credits.CellEnter += new DataGridViewCellEventHandler(dgv_credits_CellEnter);   //#1
        }

        //#1 Связываем DataGridView Debitors с TextBox'сами и с DataGridView Credits.
        private void dgv_debitors_CellEnter(object sender, DataGridViewCellEventArgs e)
        {
            //.Rows[e.RowIndex] - узнаем конкретную строку по ячейке которую выделили через.
            //.Cells[0] - указываем из какой колонки брать данные! Первая ячейка - текущей строки!
            txbx_debitorID.Text = dgv_debitors.Rows[e.RowIndex].Cells[0].Value.ToString();
            txbx_debitorName.Text = dgv_debitors.Rows[e.RowIndex].Cells[e.ColumnIndex].Value.ToString();
            txbx_debitorPostNumber.Text = dgv_debitors.Rows[e.RowIndex].Cells[e.ColumnIndex + 1].Value.ToString();

            string phone = dgv_debitors.Rows[e.RowIndex].Cells[3].Value.ToString();
            txbx_PhoneNumber.Text = (phone == String.Empty) ? "Нет Данных" : phone;

            dgv_credits.DataSource = dal.GetAllCreditsForDebitor(dgv_debitors.CurrentRow.Cells["ID"].Value.ToString());
        }

        //#1 Связываем DataGridView Credits с DataGridView Payments.
        private void dgv_credits_CellEnter(object sender, DataGridViewCellEventArgs e)
        {
            string creditorID = dgv_credits.CurrentRow.Cells["ID"].Value.ToString();
            dgv_payments.DataSource = dal.GetAllPaymentsForCredit(creditorID);
        }

        //#1
        private void MainForm_FormClosing(object sender, FormClosingEventArgs e)
        {
            if (MessageBox.Show("Вы действительно желаете закрыть приложение?", "Bank Manager", MessageBoxButtons.OKCancel) == DialogResult.OK)
                e.Cancel=false;
            else    e.Cancel=true;
        }

        //#1
        private void exitToolStripMenuItem_Click(object sender, EventArgs e)
        {
            Application.Exit();
            //this.Close();
        }

        //#2 Добавление нового дебитора DAL
        private void addNewDebitorToolStripMenuItem_Click(object sender, EventArgs e)
        {
            NewDebitor newDebitor = new NewDebitor(); //Создать обьект формы. Она напрямую связана с DAL.
            if (newDebitor.ShowDialog() == DialogResult.OK) //Модальный режим, блокирует основную форму
            {
                //dgv_debitors.DataSource = dal.GetAllDebitors(); //Сохранили Дебитора и обновили список дебиторов чтобы он в нем появился!! 
                allDebitors.Clear();                    //2.37
                allDebitors = dal.GetAllDebitors();     //2.37
                dgv_debitors.DataSource = allDebitors;  //2.37
                MessageBox.Show("Новый дебитор успешно создан.", "Банк", MessageBoxButtons.OK);
            }
            else
                MessageBox.Show("Новый дебитор не создан!!!", "Банк", MessageBoxButtons.OK);
        }

        //#2 Добавление нового Кредита UI
        private void openNewCreditToolStripMenuItem_Click(object sender, EventArgs e)
        {
            NewCredit newCredit = new NewCredit();
            if (newCredit.ShowDialog() == DialogResult.OK)
            {
                //Добавили кредит, обновили DGV Credits. Чтобы получить ID дебитора, берем его из Текущей строки, из столбца с именем "ID"
                dgv_credits.DataSource = dal.GetAllCreditsForDebitor(dgv_debitors.CurrentRow.Cells["ID"].Value.ToString());
                MessageBox.Show("Новый кредит успешно выдан.", "Банк", MessageBoxButtons.OK);
            }
            else
                MessageBox.Show("Новый кредит не выдан!!!", "Банк", MessageBoxButtons.OK);
        }
         
        //#2.2 Создание нового платежа UI
        private void passNewPaymentToolStripMenuItem_Click(object sender, EventArgs e)
        {
            NewPayment newPayment = new NewPayment();
            if (newPayment.ShowDialog() == DialogResult.OK)
            {
                //Добавили кредит, обновили DGV Credits. Чтобы получить ID дебитора, берем его из Текущей строки, из столбца с именем "ID"
                //!!! Оставляем ту же строку, т.к. при перестроении DGV Credits, перестроится тоже DGV Payments
                dgv_credits.DataSource = dal.GetAllCreditsForDebitor(dgv_debitors.CurrentRow.Cells["ID"].Value.ToString());
                MessageBox.Show("Новый платеж успешно принят.", "Банк", MessageBoxButtons.OK);
            }
            else
                MessageBox.Show("Новый платеж не принят!!!", "Банк", MessageBoxButtons.OK);
        }
        //2.27 Сохранение данных. и проверка
        private void saveDataToCSVToolStripMenuItem_Click(object sender, EventArgs e)
        {
            if (dal.SaveDBToLocalFile())
                MessageBox.Show("Данные БД были успешно сохранены в локальных файлах.", "Банк Менеджер", MessageBoxButtons.OK, MessageBoxIcon.Information, MessageBoxDefaultButton.Button1);
            else
                MessageBox.Show("Данные БД НЕ были сохранены в локальных файлах!", "Банк Менеджер", MessageBoxButtons.OK, MessageBoxIcon.Error, MessageBoxDefaultButton.Button1);
        }

        private void dgv_RowPrePaint(object sender, DataGridViewRowPrePaintEventArgs e)
        {
            //2.32
            //Заголовочная ячейка конкретной текущей строки. Универсально через sender
            object headValue = ((DataGridView)sender).Rows[e.RowIndex].HeaderCell.Value; //Получили текущее значений той ячейки которой хотим установить номер
            if (headValue == null || !headValue.Equals((e.RowIndex + 1).ToString()))  //Если знач не утсановленно, или установленно, но не то которое нам нужно
                {
                    ((DataGridView)sender).Rows[e.RowIndex].HeaderCell.Value = (e.RowIndex + 1).ToString();
                }
        }

        //2.33 Поиск BLL
        //Поле класса. Для кнопки Next!!!!
        List<DataGridViewRow> searchedRows;     //все записи которые удовлетворяют истории поиска!
        int currentRow;                        //Номер позиции в списке найденных записей
        private void btn_search_Click(object sender, EventArgs e)
        {
            //Поиск на клиенте на DGV   //каждый DGV содржит в себе список DataGridViewRow
            searchedRows = new List<DataGridViewRow>();
            string debName = txbx_searchedDebName.Text.Trim();
            string debPostNumber = txbx_searchedDebPostNumber.Text.Trim();
            string debPhoneNumber = txbx_searchedDebPhoneNumber.Text.Trim();

            if (!chbx_DB.Checked)
            {
                dgv_debitors.DataSource = allDebitors;
                //Проходимся по DGV и сравниваем с теми полями, что ввели в поля поиска!
                foreach (DataGridViewRow row in dgv_debitors.Rows)
                {
                    if (//Строка.Ячейка["Name"].ВозвратФорматирЗначЯчейки.ToString() <-получили тип String и потом только .Contains(debName) <- Содержит ли в себе знач перем из textBox'а
                        row.Cells["Name"].FormattedValue.ToString().Contains(debName) &&
                        row.Cells["PostNumber"].FormattedValue.ToString().Contains(debPostNumber) &&
                        row.Cells["PhoneNumber"].FormattedValue.ToString().Contains(debPhoneNumber))
                    {
                        searchedRows.Add(row);
                    }
                } 
                if (searchedRows.Count == 0)
                {
                    MessageBox.Show("По данному запросу не найдено записей.");
                    btn_nextRow.Enabled = false;
                    return;     //Выход из метода
                }
                MessageBox.Show("Найдено " + searchedRows.Count + " записей.");
                btn_nextRow.Enabled = true;
                currentRow = -1; //Для цикличного перемещения и для условия if (currentRow == searchedRows.Count - 1)
                btn_nextRow_Click(null, null); //При поиске автоматом попадем сразу на первую найденную запись! Просто кнопка сама нажмется программмно!
            }
            else
            {
                btn_nextRow.Enabled = false;
                ArrayList searchedDebitors = dal.SearchDebitors(debName, debPostNumber, debPhoneNumber);
                if (searchedDebitors == null || searchedDebitors.Count == 0)
                {
                    MessageBox.Show("Ничего не найдено.");
                    return;
                }
                else
                {
                    MessageBox.Show("Найдено " + searchedDebitors.Count + " записей");
                    dgv_debitors.DataSource = searchedDebitors;
                }
            }
            

        }

        //2.34 Выделяем строку найденную в запросе поиска.
        private void btn_nextRow_Click(object sender, EventArgs e)
        {
            if (currentRow == searchedRows.Count - 1) //Прим 3 записи найденно. 0 1 2, но это Count=3, поэтому Count-1 = 2. 
            {                                         //-> Т.е. когда будем на последней строке,
                currentRow = 0;                       // то обнулим countRows и начнем заново щелкать!
            }
            else 
                currentRow++;

                                                                            //dgv_debitors.CurrentCell - чтобы выделить строку - можно выделить ячейку из всей строки!
            dgv_debitors.CurrentCell = searchedRows[currentRow].Cells[1];   //Есть список найденных дебиторов. //currentRow - последоват проходмся по нему. 
                                                                            //searchedRows[currentRow]. - номер строки 
                                                                            //.Cells[1]; - номер ячейки / выделяем первую ячейку - "Name"
        }
    }
}