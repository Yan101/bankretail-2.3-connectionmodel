USE [master]
GO
/****** Object:  Database [BANK]    Script Date: 18.10.2015 19:46:00 ******/
CREATE DATABASE [BANK]
 CONTAINMENT = NONE
 ON  PRIMARY 
( NAME = N'BANK', FILENAME = N'C:\Program Files\Microsoft SQL Server\MSSQL11.MSSQLSERVER\MSSQL\DATA\BANK.mdf' , SIZE = 4160KB , MAXSIZE = UNLIMITED, FILEGROWTH = 1024KB )
 LOG ON 
( NAME = N'BANK_log', FILENAME = N'C:\Program Files\Microsoft SQL Server\MSSQL11.MSSQLSERVER\MSSQL\DATA\BANK_log.ldf' , SIZE = 1040KB , MAXSIZE = 2048GB , FILEGROWTH = 10%)
GO
ALTER DATABASE [BANK] SET COMPATIBILITY_LEVEL = 90
GO
IF (1 = FULLTEXTSERVICEPROPERTY('IsFullTextInstalled'))
begin
EXEC [BANK].[dbo].[sp_fulltext_database] @action = 'enable'
end
GO
ALTER DATABASE [BANK] SET ANSI_NULL_DEFAULT OFF 
GO
ALTER DATABASE [BANK] SET ANSI_NULLS OFF 
GO
ALTER DATABASE [BANK] SET ANSI_PADDING OFF 
GO
ALTER DATABASE [BANK] SET ANSI_WARNINGS OFF 
GO
ALTER DATABASE [BANK] SET ARITHABORT OFF 
GO
ALTER DATABASE [BANK] SET AUTO_CLOSE OFF 
GO
ALTER DATABASE [BANK] SET AUTO_CREATE_STATISTICS ON 
GO
ALTER DATABASE [BANK] SET AUTO_SHRINK OFF 
GO
ALTER DATABASE [BANK] SET AUTO_UPDATE_STATISTICS ON 
GO
ALTER DATABASE [BANK] SET CURSOR_CLOSE_ON_COMMIT OFF 
GO
ALTER DATABASE [BANK] SET CURSOR_DEFAULT  GLOBAL 
GO
ALTER DATABASE [BANK] SET CONCAT_NULL_YIELDS_NULL OFF 
GO
ALTER DATABASE [BANK] SET NUMERIC_ROUNDABORT OFF 
GO
ALTER DATABASE [BANK] SET QUOTED_IDENTIFIER OFF 
GO
ALTER DATABASE [BANK] SET RECURSIVE_TRIGGERS OFF 
GO
ALTER DATABASE [BANK] SET  ENABLE_BROKER 
GO
ALTER DATABASE [BANK] SET AUTO_UPDATE_STATISTICS_ASYNC OFF 
GO
ALTER DATABASE [BANK] SET DATE_CORRELATION_OPTIMIZATION OFF 
GO
ALTER DATABASE [BANK] SET TRUSTWORTHY OFF 
GO
ALTER DATABASE [BANK] SET ALLOW_SNAPSHOT_ISOLATION OFF 
GO
ALTER DATABASE [BANK] SET PARAMETERIZATION SIMPLE 
GO
ALTER DATABASE [BANK] SET READ_COMMITTED_SNAPSHOT OFF 
GO
ALTER DATABASE [BANK] SET HONOR_BROKER_PRIORITY OFF 
GO
ALTER DATABASE [BANK] SET RECOVERY FULL 
GO
ALTER DATABASE [BANK] SET  MULTI_USER 
GO
ALTER DATABASE [BANK] SET PAGE_VERIFY CHECKSUM  
GO
ALTER DATABASE [BANK] SET DB_CHAINING OFF 
GO
ALTER DATABASE [BANK] SET FILESTREAM( NON_TRANSACTED_ACCESS = OFF ) 
GO
ALTER DATABASE [BANK] SET TARGET_RECOVERY_TIME = 0 SECONDS 
GO
EXEC sys.sp_db_vardecimal_storage_format N'BANK', N'ON'
GO
USE [BANK]
GO
/****** Object:  StoredProcedure [dbo].[GetAllCreditsByCurrentDebitor]    Script Date: 18.10.2015 19:46:00 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[GetAllCreditsByCurrentDebitor]
	(
		@debitorID uniqueidentifier
	)
AS

	SELECT * FROM Credits Where DebitorID = @debitorID Order By OpenDate

	RETURN


GO
/****** Object:  StoredProcedure [dbo].[SearchDebitor]    Script Date: 18.10.2015 19:46:00 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[SearchDebitor]
(
		@debName nvarchar(50),
		@debPostNum nvarchar(50),
		@debPhoneNum nvarchar(50)
	)
AS

	SELECT * FROM Debitors WHERE
	Name like '%' + @debName + '%' AND
	CAST (PostNumber as nvarchar(50)) like '%' + @debPostNum + '%' AND
	(PhoneNumber like '%' + @debPhoneNum + '%' OR PhoneNumber IS NULL)
 
 RETURN
GO
/****** Object:  Table [dbo].[Credits]    Script Date: 18.10.2015 19:46:00 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[Credits](
	[ID] [uniqueidentifier] NOT NULL,
	[DebitorID] [uniqueidentifier] NOT NULL,
	[Amount] [money] NOT NULL,
	[Balance] [money] NOT NULL,
	[OpenDate] [datetime] NOT NULL,
 CONSTRAINT [PK_Credits] PRIMARY KEY CLUSTERED 
(
	[ID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
/****** Object:  Table [dbo].[Debitors]    Script Date: 18.10.2015 19:46:00 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[Debitors](
	[ID] [uniqueidentifier] NOT NULL,
	[Name] [nvarchar](50) NOT NULL,
	[PostNumber] [bigint] NOT NULL,
	[PhoneNumber] [nchar](50) NULL,
 CONSTRAINT [PK_Debitors] PRIMARY KEY CLUSTERED 
(
	[ID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
/****** Object:  Table [dbo].[Payments]    Script Date: 18.10.2015 19:46:00 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[Payments](
	[ID] [uniqueidentifier] NOT NULL,
	[CreditsID] [uniqueidentifier] NOT NULL,
	[Amount] [decimal](9, 2) NOT NULL,
	[PaymentDate] [datetime] NOT NULL,
 CONSTRAINT [PK_Payments] PRIMARY KEY CLUSTERED 
(
	[ID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
INSERT [dbo].[Credits] ([ID], [DebitorID], [Amount], [Balance], [OpenDate]) VALUES (N'64e21b1d-310b-4a32-bd14-10a28b458f33', N'e3eeeeee-eeee-eeee-eeee-eeeeeeeeeee4', 100000.0000, 100000.0000, CAST(N'2015-10-15 21:22:22.417' AS DateTime))
INSERT [dbo].[Credits] ([ID], [DebitorID], [Amount], [Balance], [OpenDate]) VALUES (N'fd78a28a-dec1-4b31-add5-6455212c9735', N'd19619dd-864c-4b55-b1d1-f3651f5e6325', 150000.0000, 149556.0000, CAST(N'2015-10-18 18:30:27.957' AS DateTime))
INSERT [dbo].[Credits] ([ID], [DebitorID], [Amount], [Balance], [OpenDate]) VALUES (N'd3ffdc18-5d05-4c18-9c4b-88f6c93751c0', N'e3eeeeee-eeee-eeee-eeee-eeeeeeeeeee4', 5000.0000, 5000.0000, CAST(N'2015-10-15 21:24:33.533' AS DateTime))
INSERT [dbo].[Credits] ([ID], [DebitorID], [Amount], [Balance], [OpenDate]) VALUES (N'c4c2d9a6-eed0-425d-b211-bdf3764b9011', N'e44eeeee-eeee-eeee-eeee-eeeeeeeeeeee', 19999.0000, 19444.0000, CAST(N'2015-10-10 01:34:05.000' AS DateTime))
INSERT [dbo].[Credits] ([ID], [DebitorID], [Amount], [Balance], [OpenDate]) VALUES (N'360abd89-bbaa-4475-999c-bef839a3ed08', N'e44eeeee-eeee-eeee-eeee-eeeeeeeeeeee', 16666.0000, 16666.0000, CAST(N'2015-12-10 01:34:20.000' AS DateTime))
INSERT [dbo].[Credits] ([ID], [DebitorID], [Amount], [Balance], [OpenDate]) VALUES (N'3e1d8aa4-1843-49f6-9b6b-f2407dae978e', N'e44eeeee-eeee-eeee-eeee-eeeeeeeeeeee', 166666.0000, 166666.0000, CAST(N'2015-10-10 01:35:08.000' AS DateTime))
INSERT [dbo].[Credits] ([ID], [DebitorID], [Amount], [Balance], [OpenDate]) VALUES (N'99aa645e-cefa-430a-a322-f4e4c12b2ca6', N'e44eeeee-eeee-eeee-eeee-eeeeeeeeeeee', 150000.0000, 149850.0000, CAST(N'2015-01-10 01:29:23.000' AS DateTime))
INSERT [dbo].[Credits] ([ID], [DebitorID], [Amount], [Balance], [OpenDate]) VALUES (N'99aa645e-cefa-430a-a322-f4e4c12b2ca7', N'e44eeeee-eeee-eeee-eeee-eeeeeeeeeeee', 1200000000.0000, 1200000000.0000, CAST(N'2015-01-10 00:00:00.000' AS DateTime))
INSERT [dbo].[Credits] ([ID], [DebitorID], [Amount], [Balance], [OpenDate]) VALUES (N'99aa645e-cefa-430a-a322-f4e4c12b2ca8', N'e44eeeee-eeee-eeee-eeee-eeeeeeeeeeee', 160000.0000, 160000.0000, CAST(N'2015-01-10 00:00:00.000' AS DateTime))
INSERT [dbo].[Credits] ([ID], [DebitorID], [Amount], [Balance], [OpenDate]) VALUES (N'99aa645e-cefa-430a-a322-f4e4c12b2ca9', N'e44eeeee-eeee-eeee-eeee-eeeeeeeeeeee', 156000.0000, 156000.0000, CAST(N'2015-01-15 00:00:00.000' AS DateTime))
INSERT [dbo].[Debitors] ([ID], [Name], [PostNumber], [PhoneNumber]) VALUES (N'e2eeeeee-eeee-eeee-eeee-eeeeeeeeeee2', N'Петренко Анна Александровна', 32424, N'003 8 093 345-78-02                               ')
INSERT [dbo].[Debitors] ([ID], [Name], [PostNumber], [PhoneNumber]) VALUES (N'e3eeeeee-eeee-eeee-eeee-eeeeeeeeeee4', N'Радужный Алексей Иванович', 32454654, N'33                                                ')
INSERT [dbo].[Debitors] ([ID], [Name], [PostNumber], [PhoneNumber]) VALUES (N'e44eeeee-eeee-eeee-eeee-eeeeeeeeeeee', N'Гудок Александр Сергеевич', 3242345, N'003 8 067 895-96-03                               ')
INSERT [dbo].[Debitors] ([ID], [Name], [PostNumber], [PhoneNumber]) VALUES (N'd19619dd-864c-4b55-b1d1-f3451f5e6325', N'z3ок', 98739, NULL)
INSERT [dbo].[Debitors] ([ID], [Name], [PostNumber], [PhoneNumber]) VALUES (N'd19619dd-864c-4b55-b1d1-f3651f5e6325', N'z2', 133323, N'                                                  ')
INSERT [dbo].[Payments] ([ID], [CreditsID], [Amount], [PaymentDate]) VALUES (N'431150eb-caec-4898-863d-714a3d5d40cb', N'99aa645e-cefa-430a-a322-f4e4c12b2ca6', CAST(150.00 AS Decimal(9, 2)), CAST(N'2015-06-11 23:12:41.000' AS DateTime))
INSERT [dbo].[Payments] ([ID], [CreditsID], [Amount], [PaymentDate]) VALUES (N'af625a77-99bb-4ce6-b4df-83b08b468361', N'fd78a28a-dec1-4b31-add5-6455212c9735', CAST(444.00 AS Decimal(9, 2)), CAST(N'2015-06-10 18:31:37.000' AS DateTime))
INSERT [dbo].[Payments] ([ID], [CreditsID], [Amount], [PaymentDate]) VALUES (N'3c78ffe1-e93b-40f8-bc5e-e8d625427017', N'c4c2d9a6-eed0-425d-b211-bdf3764b9011', CAST(555.00 AS Decimal(9, 2)), CAST(N'2015-10-10 20:20:20.000' AS DateTime))
ALTER TABLE [dbo].[Credits]  WITH CHECK ADD  CONSTRAINT [FK_Credits_Debitors] FOREIGN KEY([DebitorID])
REFERENCES [dbo].[Debitors] ([ID])
GO
ALTER TABLE [dbo].[Credits] CHECK CONSTRAINT [FK_Credits_Debitors]
GO
ALTER TABLE [dbo].[Payments]  WITH CHECK ADD  CONSTRAINT [FK_Payments_Credits] FOREIGN KEY([CreditsID])
REFERENCES [dbo].[Credits] ([ID])
GO
ALTER TABLE [dbo].[Payments] CHECK CONSTRAINT [FK_Payments_Credits]
GO
USE [master]
GO
ALTER DATABASE [BANK] SET  READ_WRITE 
GO
